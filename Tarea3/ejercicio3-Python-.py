#encoding=UTF-8
import subprocess
from subprocess import PIPE
import sys
from complemento import resumido, resumido2

if __name__ == "__main__":
	accion = sys.argv[1]
	accion2 = sys.argv[1]
	accion3 = sys.argv[1]
	
	if accion == "crear":
		subprocess.Popen(resumido, shell=True)

	if accion2 == "pruebadeuso":
		subprocess.Popen(resumido2, shell=True)
	
	if accion3 == "pipe":
		zymvol1 = subprocess.Popen("more Estudiantes", shell=True, stdout = PIPE)
		zymvol2 = subprocess.Popen("grep -e Cristian -e Joaquin -e Jon -e Pedro", stdin = zymvol1.stdout, stdout = PIPE, shell=True)
		zymvol1.stdout.close()
		output = zymvol2.communicate()[0]
		print (output)
		
